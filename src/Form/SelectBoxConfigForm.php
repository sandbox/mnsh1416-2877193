<?php

namespace Drupal\select_box\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * General configuration form for controlling the SelectBox behaviour..
 */
class SelectBoxConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'select_box_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['select_box.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('select_box.settings');

    $form['select_box_custom_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Styles and options'),
      '#open' => TRUE,
    ];

    $form['select_box_custom_settings']['selectbox_mobile'] = [
      '#type' => 'checkbox',
      '#title' => 'On Mobile',
      '#default_value' => $config->get('sb.mobile'),
      '#description' => $this->t('Disables the widget for mobile devices.'),
    ];

    $selectbox_styles = [
      'default' => $this->t('Default'),
      'slide' => $this->t('Slide'),
      'fade' => $this->t('Fade'),
    ];

    $form['select_box_custom_settings']['selectbox_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu Transition'),
      '#options' => $selectbox_styles,
      '#default_value' => $config->get('sb.menu_transition'),
      '#description' => $this->t('The show/hide transition for dropdown menus.'),
    ];

    $selectbox_menu_speed = [
      'slow' => $this->t('Slow'),
      'normal' => $this->t('Normal'),
      'fast' => $this->t('Fast'),
    ];

    $form['select_box_custom_settings']['selectbox_speed'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu Speed'),
      '#options' => $selectbox_menu_speed,
      '#default_value' => $config->get('sb.menu_speed'),
      '#description' => $this->t('The show/hide transition speed.'),
    ];

    $form['select_box_custom_settings']['selectbox_loop'] = [
      '#type' => 'checkbox',
      '#title' => 'Loop Options',
      '#default_value' => $config->get('sb.loop'),
      '#description' => $this->t('Flag to allow arrow keys to loop through options.'),
    ];

    $form['select_box_custom_settings']['selectbox_window_scroll'] = [
      '#type' => 'checkbox',
      '#title' => 'Hide On Window Scroll',
      '#default_value' => $config->get('sb.window_scroll'),
      '#description' => $this->t('If false then showed droplist will not hide itself on window scroll event.'),
    ];

    $form['select_box_custom_settings']['selectbox_keep_viewport'] = [
      '#type' => 'checkbox',
      '#title' => 'Keep In Viewport',
      '#default_value' => $config->get('sb.keep_viewport'),
      '#description' => $this->t('If set to false, the droplist will be always open towards the bottom.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->getEditable('select_box.settings');

    $config
      ->set('sb.mobile', $form_state->getValue('selectbox_mobile'))
      ->set('sb.menu_transition', $form_state->getValue('selectbox_transition'))
      ->set('sb.menu_speed', $form_state->getValue('selectbox_speed'))
      ->set('sb.loop', $form_state->getValue('selectbox_loop'))
      ->set('sb.window_scroll', $form_state->getValue('selectbox_window_scroll'))
      ->set('sb.keep_viewport', $form_state->getValue('selectbox_keep_viewport'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
