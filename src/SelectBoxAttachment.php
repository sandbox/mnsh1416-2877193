<?php

namespace Drupal\select_box;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * An implementation of PageAttachmentInterface for the SelectBox library.
 */
class SelectBoxAttachment {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The SelectBox settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Create an instance of SelectBoxAttachment.
   */
  public function __construct(ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config) {
    $this->moduleHandler = $module_handler;
    $this->settings = $config->get('select_box.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$page) {

    $js_settings = [
      'mobile' => $this->settings->get('sb.mobile') ? TRUE : FALSE,
      'menuTransition' => $this->settings->get('sb.menu_transition'),
      'menuSpeed' => $this->settings->get('sb.menu_speed'),
      'loopOptions' => $this->settings->get('sb.loop') ? TRUE : FALSE,
      'hideOnWindowScroll' => $this->settings->get('sb.window_scroll') ? TRUE : FALSE,
      'keepInViewport' => $this->settings->get('sb.keep_viewport') ? TRUE : FALSE,
    ];

    // Give other modules the possibility to override SelectBox
    // settings.
    $this->moduleHandler->alter('select_box_settings', $js_settings);

    // Add SelectBox js settings.
    $page['#attached']['drupalSettings']['select_box'] = $js_settings;

    // Adding Select Box library.
    $page['#attached']['library'][] = 'select_box/select_box';
  }

}
