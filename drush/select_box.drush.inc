<?php

/**
 * @file
 * Drush integration for select box.
 */

/**
 * The Select Box plugin URI.
 */
define('SELECT_BOX_DOWNLOAD_URI', 'https://github.com/marcj/jquery-selectBox/archive/master.zip');
define('SELECT_BOX_DOWNLOAD_PREFIX', 'jquery-selectBox-');

/**
 * Implements hook_drush_command().
 */
function select_box_drush_command() {
  $items = [];

  // The key in the $items array is the name of the command.
  $items['select-box-plugin'] = [
    'callback' => 'drush_select_box_plugin',
    'description' => dt('Download and install the SelectBox plugin.'),
    // No bootstrap.
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => [
      'path' => dt('Optional. A path where to install the SelectBox plugin. If omitted Drush will use the default location.'),
    ],
    'aliases' => ['selectboxplugin'],
  ];

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 */
function select_box_drush_help($section) {
  switch ($section) {
    case 'drush:select-box-plugin':
      return dt('Download and install the Select Box plugin from marcj/jquery-selectBox, default location is the libraries directory.');
  }
}

/**
 * Command to download the Select Box plugin.
 */
function drush_select_box_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', ['@path' => $path]), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(SELECT_BOX_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = SELECT_BOX_DOWNLOAD_PREFIX . basename($filepath, '.zip');

    // Remove any existing Colorbox plugin directory.
    if (is_dir($dirname) || is_dir('select-box')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('select-box', TRUE);
      drush_log(dt('A existing Select Box plugin was deleted from @path', ['@path' => $path]), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename);

    // Change the directory name to "select-box" if needed.
    if ($dirname != 'select-box') {
      drush_move_dir($dirname, 'select-box', TRUE);
      $dirname = 'select-box';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('Select Box plugin has been installed in @path', ['@path' => $path]), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the Select Box plugin to @path', ['@path' => $path]), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
