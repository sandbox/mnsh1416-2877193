/**
 * @file
 * Javascript to provide SelectBox look'n'feel
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.initSelectBox = {
    attach: function (context, settings) {
      $('.select-box', context)
              .selectBox(settings.select_box);
    }
  };

})(jQuery, Drupal);
