CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers



INTRODUCTION
------------

This module changes the look and feel of Drupal 8
defaults lists. List can be of any type: 
lists integer, lists Text, Lists Float. Look and 
Feel will be same thought out the site.

Features
------------

This allow admin/editor to configure the style of lists 
via "configure" this module at "/admin/config/user-interface/select-box"
From here you can choose so many available options like:
Show on mobile, Loop, Speed, Transition. 

Requirements
------------

This module requires: 
* Drupal Libraries module (http://drupal.org/project/libraries)
* jQuery selectBox (https://github.com/marcj/jquery-selectBox) 

Installation
------------

1) Create a libraries/ directory on your server
2) Create a directory within libraries/ named "select-box"
3) Download the latest jQuery selectBox plugin from
   https://github.com/marcj/jquery-selectBox
4) Enable the jQuery Select Box module

Configuration
------------
Configure your settings for SelectBox at /admin/config/user-interface/select-box
with an admin configuration settings, you can change the effect of select list
through out the site.

DRUSH
------

A Drush command is provides for easy installation of the Select Box
Plugin itself.

% drush select-box-plugin

The command will download the plugin and unpack it in "libraries/".
It is possible to add another path as an option to the command, but not
recommended unless you know what you are doing.

MAINTAINERS
-----------

Current maintainers:

 * Manish Jatwani (mnsh1416) - https://www.drupal.org/user/3122333

Requires - Drupal 8
License - GPL (see LICENSE)
